FROM python:3.11-slim

WORKDIR /app

COPY poetry.lock .
COPY pyproject.toml .

# Install build dependencies
RUN apt-get update \
  && apt-get install -y \
    postgresql-client \
    gcc \
    libpq-dev \
    libffi-dev \
    netcat-openbsd \
    curl \
    iproute2 \
    iputils-ping \
    procps \
  && pip install --upgrade pip poetry \
  && poetry config virtualenvs.create false \
  && MAKEFLAGS="-j $(nproc)" poetry install \
  && apt-get autoremove -y --purge libpq-dev gcc \
  && apt-get clean \
  && rm -rf ${HOME}/.cache /var/cache/apt

RUN addgroup --gid 1000 --system app \
  && adduser --system --uid 1000 --ingroup app app

COPY . /app/
COPY .container-files/web /

User app

EXPOSE 8000

ENTRYPOINT ["/app/scripts/entrypoint"]

CMD ["gunicorn", "--chdir=/app/src", "korra.wsgi:application", "--config", "/app/gunicorn.conf", "--log-config", "/app/logging.conf", "-b", ":8000"]
