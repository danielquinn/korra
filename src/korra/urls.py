from django.contrib import admin
from django.urls import path, re_path
from django.views.generic import TemplateView

from files.views import DownloadView, UploadView, UploadedView


urlpatterns = [
    path("", UploadView.as_view(), name="upload"),
    path("success", UploadedView.as_view(), name="uploaded"),
    re_path("^download/(?P<pk>.*)$", DownloadView.as_view(), name="download"),
    path(
        "about/",
        TemplateView.as_view(template_name="korra/about.html"),
        name="about",
    ),
    path("admin/", admin.site.urls),
]
